import os

import logging
from flask_sqlalchemy import SQLAlchemy as _BaseSQLAlchemy

from YtApi.common.config_accessor import ConfigAccessor

logger = logging.getLogger(__name__)


# use pool_pre_ping to avoid disconnect exception raised
class SQLAlchemy(_BaseSQLAlchemy):
    def apply_pool_defaults(self, app, options):
        super(SQLAlchemy, self).apply_pool_defaults(app, options)
        options["pool_pre_ping"] = True


class Connector(object):
    @classmethod
    def init_db(cls, flask_app):
        try:
            config_dict = ConfigAccessor.config_access("db")
            host = config_dict['host']
            user = config_dict['user']

            # retrieve the database password from environment
            password = os.environ[config_dict['password_key']]
            database = config_dict['database']

            db_uri = 'mysql+pymysql://{}:{}@{}/{}'.format(user, password, host, database)

            flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
            flask_app.config['SQLALCHEMY_DATABASE_URI'] = db_uri

            # Base = declarative_base()
            cls.db = SQLAlchemy(flask_app)
            # self.db = Base

            # Base.metadata.create_all(bind=engine)
            logger.debug('init db')
        except Exception as e:
            # avoid MASSIVE error messages when docker compose starts, and databases not still up
            logger.error(str(e))
            logger.error('database down')
