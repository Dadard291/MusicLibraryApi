from flask import Blueprint
from flask_restplus import Api

# init the REST API nested into the flask application with the blueprint
from YtApi.common.config_accessor import ConfigAccessor

config = ConfigAccessor.config_access('api')

api_v1 = Blueprint('api', __name__, url_prefix=config['url_prefix'])

api = Api(
    api_v1,
    title=config['title'],
    doc=config['url_doc'],
    version=config['version'],
    description=config['description'],
    contact_email=config['contact'],
    license=config['license'],
    license_url=config['license_url']
)

profile_key_authorization = {
    "api_key": {
        "type": "apiKey",
        "in": "query",
        "name": "api_key"
    }
}

# define the namespaces
common_ns = api.namespace('common', description='requirements to be bind with the main API',
                          authorizations=profile_key_authorization)
video_ns = api.namespace('video', description='video management', authorizations=profile_key_authorization)

# execute the controllers files, in order to register the routes to the namespaces
from YtApi.controllers.videos_controller import *
from YtApi.controllers.common_controller import *

