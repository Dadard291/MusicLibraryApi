from http import HTTPStatus


class DalException(Exception):

    def __init__(self, msg, status_code=HTTPStatus.INTERNAL_SERVER_ERROR):
        self.msg = msg
        self.status_code = int(status_code)


class BusinessException(Exception):

    def __init__(self, msg, status_code=HTTPStatus.INTERNAL_SERVER_ERROR):
        self.msg = msg
        self.status_code = int(status_code)