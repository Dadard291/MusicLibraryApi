def format_error(msg):
    return dict(status='ko', message=msg, content={})


def format_response(msg, payload):
    return dict(status='ok', message=msg, content=payload)


def check_query_parameter(*args):
    for a in args:
        if a is None or a == "":
            return False

    return True
