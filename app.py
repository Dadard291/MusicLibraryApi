from flask import Flask
from flask_cors import CORS
import os

from YtApi.dal.repository.connector import Connector

# init the main flask application
flask_app = Flask(__name__)

# initiate the database connector
Connector.init_db(flask_app)

from YtApi.host.host import api_v1
from waitress import serve

# register the api with the blueprint
flask_app.register_blueprint(api_v1)

# avoid cors policy restriction when request received
CORS(flask_app)

# RUN, U FOOLS
if __name__ == '__main__':
    if os.environ["API_ENV"] == "PROD":
        serve(flask_app, listen="*:5000")
    else:
        flask_app.run()  # DEV MODE ONLY
