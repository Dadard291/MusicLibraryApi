FROM python:latest

WORKDIR /usr/src/app

ARG MYSQL_MAIN_PASSWORD
ARG YT_API_KEY

COPY requirements.txt ./

RUN apt-get clean
RUN apt-get update

RUN apt-get install -y ffmpeg

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

# change default favicon
COPY ./static/static/* /usr/local/lib/python3.7/site-packages/flask_restplus/static/
COPY ./static/templates/* /usr/local/lib/python3.7/site-packages/flask_restplus/templates/


ENV API_ENV=PROD
ENV MYSQL_MAIN_PASSWORD=$MYSQL_MAIN_PASSWORD
ENV YT_API_KEY=$YT_API_KEY

COPY . .

USER root
RUN chmod a+w ./logs

CMD [ "python", "./app.py" ]
